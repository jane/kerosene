package pm.j4.kerosene.gui;

import com.mojang.blaze3d.systems.RenderSystem;
import java.util.Map;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.render.BufferBuilder;
import net.minecraft.client.render.Tessellator;
import net.minecraft.client.render.VertexFormats;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.text.LiteralText;
import net.minecraft.text.TranslatableText;
import pm.j4.kerosene.modules.menu.ModMenu;
import pm.j4.kerosene.util.config.ConfigManager;
import pm.j4.kerosene.util.data.ButtonInformation;
import pm.j4.kerosene.util.data.Category;

/**
 * The type P mod menu screen.
 */
public class PModMenuScreen extends Screen {
	/**
	 * Instantiates a new P mod menu screen.
	 */
	public PModMenuScreen() {
		super(new TranslatableText("petroleum.modmenu"));
	}

	@Override
	public void render(MatrixStack matrices, int mouseX, int mouseY, float delta) {
		this.renderBackground(matrices);
		super.render(matrices, mouseX, mouseY, delta);
	}

	@Override
	protected void init() {
		Map<String, ButtonInformation> coordinateMap = ModMenu.getButtons();
		coordinateMap.forEach((category, coord) -> {
			this.addButton(new PMovableButton((int) (coord.x * this.width),
					(int) (coord.y * this.height),
					category,
					Category.getByCategory(category),
					coord.open,
					this));
		});
	}

	@Override
	public void onClose() {
		if (ConfigManager.getConfig().isPresent()) {
			ConfigManager.getConfig().get().disableModule("petroleum.modmenu");
			this.buttons.forEach(button -> ((PMovableButton) button).updateCoordinate());
			ConfigManager.saveGlobalConfig();
		}
		super.onClose();
	}

	@Override
	public void renderBackground(MatrixStack matrices) {
		Tessellator t_1 = Tessellator.getInstance();
		BufferBuilder buffer = t_1.getBuffer();
		RenderSystem.enableBlend();
		buffer.begin(7, VertexFormats.POSITION_COLOR);
		buffer.vertex(0, this.height, 0.0D).color(0.1F, 0.1F, 0.1F, 0.3F).next();
		buffer.vertex(this.width, this.height, 0.0D).color(0.1F, 0.1F, 0.1F, 0.3F).next();
		buffer.vertex(this.width, 0, 0.0D).color(0.1F, 0.1F, 0.1F, 0.3F).next();
		buffer.vertex(0, 0, 0.0D).color(0.1F, 0.1F, 0.1F, 0.3F).next();
		t_1.draw();
		RenderSystem.disableBlend();
	}
}
