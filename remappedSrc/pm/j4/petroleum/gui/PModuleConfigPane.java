package pm.j4.kerosene.gui;

import com.mojang.blaze3d.platform.GlStateManager;
import com.mojang.blaze3d.systems.RenderSystem;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.font.TextRenderer;
import net.minecraft.client.gui.widget.EntryListWidget;
import net.minecraft.client.render.BufferBuilder;
import net.minecraft.client.render.Tessellator;
import net.minecraft.client.render.VertexFormats;
import net.minecraft.client.util.math.MatrixStack;

/**
 * The type P module config pane.
 */
public class PModuleConfigPane extends EntryListWidget<PModuleConfigEntry> {
	/**
	 * The Parent.
	 */
	private final POptionsScreen parent;
	/**
	 * The Last selected.
	 */
	private POptionEntry lastSelected;

	/**
	 * Instantiates a new P module config pane.
	 *
	 * @param client      the client
	 * @param width       the width
	 * @param height      the height
	 * @param top         the top
	 * @param bottom      the bottom
	 * @param entryHeight the entry height
	 * @param screen      the screen
	 */
	public PModuleConfigPane(MinecraftClient client, int width, int height, int top, int bottom, int entryHeight, POptionsScreen screen) {
		super(client, width, height, top, bottom, entryHeight);
		this.parent = screen;
		/**
		 * The Text renderer.
		 */
		TextRenderer textRenderer = client.textRenderer;
	}

	@Override
	public PModuleConfigEntry getSelected() {
		return null;
	}

	@Override
	public int getRowWidth() {
		return this.width - 10;
	}

	@Override
	protected int getScrollbarPositionX() {
		return this.width - 6 + left;
	}

	@Override
	public void render(MatrixStack matrices, int mouseX, int mouseY, float delta) {
		POptionEntry selectedEntry = parent.getSelected();
		if (selectedEntry != lastSelected) {
			lastSelected = selectedEntry;
			clearEntries();
			setScrollAmount(-Double.MAX_VALUE);
			String id = lastSelected.getModId();
			if (lastSelected != null && id != null && !id.isEmpty()) {
				children().addAll(lastSelected.module.getConfigEntries());
			}
		}

		Tessellator t_1 = Tessellator.getInstance();
		BufferBuilder buffer = t_1.getBuffer();

		RenderSystem.depthFunc(515);
		RenderSystem.disableDepthTest();
		RenderSystem.enableBlend();
		RenderSystem.blendFuncSeparate(GlStateManager.SrcFactor.SRC_ALPHA,
				GlStateManager.DstFactor.ONE_MINUS_DST_ALPHA,
				GlStateManager.SrcFactor.ZERO,
				GlStateManager.DstFactor.ONE);
		RenderSystem.disableAlphaTest();
		RenderSystem.shadeModel(7425);
		RenderSystem.disableTexture();

		buffer.begin(7, VertexFormats.POSITION_COLOR_TEXTURE);
		buffer.vertex(this.left, (this.top + 4), 0.0D).texture(0.0F, 1.0F).color(0, 0, 0, 0).next();
		buffer.vertex(this.right, (this.top + 4), 0.0D).texture(1.0F, 1.0F).color(0, 0, 0, 0).next();
		buffer.vertex(this.right, this.top, 0.0D).texture(1.0F, 0.0F).color(0, 0, 0, 255).next();
		buffer.vertex(this.left, this.top, 0.0D).texture(0.0F, 0.0F).color(0, 0, 0, 255).next();
		buffer.vertex(this.left, this.bottom, 0.0D).texture(0.0F, 1.0F).color(0, 0, 0, 255).next();
		buffer.vertex(this.right, this.bottom, 0.0D).texture(1.0F, 1.0F).color(0, 0, 0, 255).next();
		buffer.vertex(this.right, (this.bottom - 4), 0.0D).texture(1.0F, 0.0F).color(0, 0, 0, 0).next();
		buffer.vertex(this.left, (this.bottom - 4), 0.0D).texture(0.0F, 0.0F).color(0, 0, 0, 0).next();
		t_1.draw();

		buffer.begin(7, VertexFormats.POSITION_COLOR);
		buffer.vertex(this.left, this.bottom, 0.0D).color(0, 0, 0, 128).next();
		buffer.vertex(this.right, this.bottom, 0.0D).color(0, 0, 0, 128).next();
		buffer.vertex(this.right, this.top, 0.0D).color(0, 0, 0, 128).next();
		buffer.vertex(this.left, this.top, 0.0D).color(0, 0, 0, 128).next();
		t_1.draw();

		int rl = this.getRowLeft();
		int sc = this.top + 4 - (int) this.getScrollAmount();
		this.renderList(matrices, rl, sc, mouseX, mouseY, delta);

		RenderSystem.enableTexture();
		RenderSystem.shadeModel(7424);
		RenderSystem.enableAlphaTest();
		RenderSystem.disableBlend();
	}
}
