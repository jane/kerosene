package pm.j4.kerosene;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import net.fabricmc.api.ModInitializer;
import net.fabricmc.fabric.api.client.event.lifecycle.v1.ClientTickEvents;
import net.fabricmc.loader.api.FabricLoader;
import net.fabricmc.loader.api.ModContainer;
import net.fabricmc.loader.api.metadata.ModMetadata;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.options.KeyBinding;
import net.minecraft.server.integrated.IntegratedServer;
import pm.j4.kerosene.modules.ExampleModule;
import pm.j4.kerosene.modules.bindings.BindingManager;
import pm.j4.kerosene.modules.list.ModList;
import pm.j4.kerosene.modules.menu.ModMenu;
import pm.j4.kerosene.modules.splash.SplashText;
import pm.j4.kerosene.modules.xray.Xray;
import pm.j4.kerosene.util.config.ConfigHolder;
import pm.j4.kerosene.util.config.ConfigManager;
import pm.j4.kerosene.util.module.ModuleBase;


//TODO:
// petroleum module checklist
// [ ] xray (lol)
// [ ] combat stuff. killaura, anti knockback, etc
// [ ] render stuff. tracers, nametags
// [ ] wurst taco. but a fish
// [ ] elytra fly
// [ ] movement stuff. nofall, jesus, speed
// [ ] elytra bhop
// [ ] boatfly
// [ ] anti anti cheat

/**
 * The type Petroleum mod.
 */
public class PetroleumMod implements ModInitializer {
	/**
	 * The Mod data.
	 */
	public static ModMetadata modData = null;
	/**
	 * The constant client.
	 */
	private static MinecraftClient client;
	/**
	 * The constant activeMods.
	 */
	private static final List<ModuleBase> activeMods = Arrays.asList(
			new SplashText(),
			new ModMenu(),
			new ModList(),
			new BindingManager(),
			new ExampleModule(),
			new Xray()
	);

	/**
	 * Is active boolean.
	 *
	 * @param modName the mod name
	 * @return the boolean
	 */
	public static boolean isActive(String modName) {
		return activeMods.stream().anyMatch(mod -> mod.getModuleName().equals(modName));
	}

	/**
	 * Gets mod.
	 *
	 * @param modName the mod name
	 * @return the mod
	 */
	public static Optional<ModuleBase> getMod(String modName) {
		return activeMods.stream().filter(mod -> mod.getModuleName().equals(modName)).findFirst();
	}

	/**
	 * Gets active mods.
	 *
	 * @return the active mods
	 */
	public static List<ModuleBase> getActiveMods() {
		return activeMods;
	}

	/**
	 * The constant registeredBinds.
	 */
	private static final List<KeyBinding> registeredBinds = new ArrayList<>();

	/**
	 * Add bind.
	 *
	 * @param b the b
	 */
	public static void addBind(KeyBinding b) {
		registeredBinds.add(b);
	}

	/**
	 * Remove bind.
	 *
	 * @param b the b
	 */
	public static void removeBind(KeyBinding b) {
		registeredBinds.remove(b);
	}

	/**
	 * Gets active keybinds.
	 *
	 * @return the active keybinds
	 */
	public static List<KeyBinding> getActiveKeybinds() {
		return registeredBinds;
	}

	/**
	 * Gets server address.
	 *
	 * @return the server address
	 */
	public static String getServerAddress() {
		if (client != null && client.getServer() != null) {
			IntegratedServer server = client.getServer();
			if (!server.isRemote()) {
				return "localhost";
			}
			if (server.isRemote() && !server.getServerIp().isEmpty()) {
				return server.getServerIp();
			}
		}
		return null;
	}

	@Override
	public void onInitialize() {

		ConfigManager.initConfig();

		// always update mod data
		Optional<ModContainer> modContainer = FabricLoader.getInstance().getModContainer("petroleum");
		modContainer.ifPresent(container -> modData = container.getMetadata());

		Optional<ConfigHolder> conf = ConfigManager.getConfig();


		//initialize any keybinds, data, etc.
		activeMods.forEach(ModuleBase::init);

		//initialize keybind handler
		conf.ifPresent(configHolder -> ClientTickEvents.END_CLIENT_TICK.register(client -> {
			if (PetroleumMod.client != client) {
				PetroleumMod.client = client;
			}
			for (KeyBinding b : PetroleumMod.getActiveKeybinds()) {
				while (b.wasPressed()) {
					configHolder.globalConfig.bindings.get(b).activate(client);
				}
			}
		}));
	}
}
