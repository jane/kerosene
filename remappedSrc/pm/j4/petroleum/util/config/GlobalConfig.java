package pm.j4.kerosene.util.config;

import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;
import net.minecraft.client.options.KeyBinding;
import net.minecraft.client.util.InputUtil;
import pm.j4.kerosene.PetroleumMod;
import pm.j4.kerosene.modules.bindings.BindingInfo;
import pm.j4.kerosene.util.data.ButtonInformation;
import pm.j4.kerosene.util.data.OptionSerializiable;
import pm.j4.kerosene.util.module.ModuleBase;
import pm.j4.kerosene.util.module.option.ConfigurationOption;

/**
 * The type Global config.
 */
public class GlobalConfig extends Config {
	/**
	 * The Bindings.
	 */
	public final Map<KeyBinding, ModuleBase> bindings = new HashMap<>();

	/**
	 * Is bound boolean.
	 *
	 * @param func the func
	 * @return the boolean
	 */
	public boolean isBound(ModuleBase func) {
		AtomicBoolean found = new AtomicBoolean(false);
		bindings.forEach((key, binding) -> {
			if (binding.equals(func)) {
				found.set(true);
			}
		});
		return found.get();
	}

	/**
	 * Sets binding.
	 *
	 * @param bind the bind
	 * @param func the func
	 */
	public void setBinding(KeyBinding bind, ModuleBase func) {
		if (bindings.containsValue(func)) {
			bindings.forEach((key, binding) -> {
				if (binding.equals(func)) {
					PetroleumMod.removeBind(key);
					bindings.remove(key);
				}
			});
		}

		if (PetroleumMod.isActive(func.getModuleName())) {
			PetroleumMod.addBind(bind);
			bindings.put(bind, func);
		}
	}

	/**
	 * Convert binding.
	 *
	 * @param info the info
	 */
	private void convertBinding(BindingInfo info) {
		Optional<ModuleBase> match = PetroleumMod.getMod(info.attachedModuleName);
		match.ifPresent(moduleBase -> setBinding(reconstructBinding(info),
				moduleBase));
	}

	/**
	 * Reconstruct binding key binding.
	 *
	 * @param info the info
	 * @return the key binding
	 */
	public static KeyBinding reconstructBinding(BindingInfo info) {
		return new KeyBinding(
				info.translationKey,
				info.type,
				info.key,
				info.category
		);
	}

	/**
	 * Extract binding info.
	 *
	 * @param b the b
	 * @param f the f
	 * @return the binding info
	 */
	public static BindingInfo extractBinding(KeyBinding b, ModuleBase f) {
		BindingInfo res = new BindingInfo();
		res.attachedModuleName = f.getModuleName();

		res.translationKey = b.getTranslationKey();
		InputUtil.Key k = b.getDefaultKey();
		res.type = k.getCategory();
		res.key = k.getCode();
		res.category = b.getCategory();

		return res;
	}

	/**
	 * Serialize bindings list.
	 *
	 * @return the list
	 */
	public List<BindingInfo> serializeBindings() {
		List<BindingInfo> b = new ArrayList<>();
		bindings.forEach((k, f) -> b.add(extractBinding(k, f)));
		return b;
	}

	/**
	 * Deserialize bindings.
	 *
	 * @param info the info
	 */
	public void deserializeBindings(List<BindingInfo> info) {
		info.forEach(this::convertBinding);
	}

	/**
	 * Serialize module configuration list.
	 *
	 * @param module the module
	 * @return the list
	 */
	public static Map<String, OptionSerializiable> serializeModuleConfiguration(ModuleBase module) {
		Map<String, OptionSerializiable> opts = new HashMap<>();
		Map<String, ConfigurationOption> configuration = module.getModuleConfiguration();
		configuration.forEach((key, value) -> {
			opts.put(key, new OptionSerializiable(key, value.toJson()));
		});
		return opts;
	}

	/**
	 * Deserialize module configuration.
	 *
	 * @param opts   the opts
	 * @param module the module
	 */
	public void deserializeModuleConfiguration(List<OptionSerializiable> opts, ModuleBase module) {
		opts.forEach(option -> {
			if (module.hasOption(option.key)) {
				module.setConfigOption(option.key, option.value);
			}
		});
	}

	/**
	 * The Button locations.
	 */
	private final Map<String, ButtonInformation> buttonLocations = new HashMap<>();

	/**
	 * Sets button.
	 *
	 * @param category          the category
	 * @param buttonInformation the button information
	 */
	public void setButton(String category, ButtonInformation buttonInformation) {
		if (buttonLocations.containsKey(category)) {
			buttonLocations.replace(category, buttonInformation);
		} else {
			buttonLocations.put(category, buttonInformation);
		}
	}

	/**
	 * Gets button.
	 *
	 * @param category the category
	 * @return the button
	 */
	public ButtonInformation getButton(String category) {
		if (buttonLocations.containsKey(category)) {
			return buttonLocations.get(category);
		}
		System.out.println("Could not find button of category " + category);
		return null;
	}
}

