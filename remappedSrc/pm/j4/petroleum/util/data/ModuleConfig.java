package pm.j4.kerosene.util.data;

import java.util.Map;

/**
 * The type Module config.
 */
public class ModuleConfig {
	/**
	 * The Options.
	 */
	public Map<String, OptionSerializiable> options;
}
