package pm.j4.kerosene.util.module.option;

import com.google.gson.JsonElement;
import com.google.gson.JsonPrimitive;

/**
 * The type Integer value.
 */
public class IntegerOption extends ConfigurationOption {
	/**
	 * The Value.
	 */
	private int value;

	/**
	 * Instantiates a new Integer option.
	 *
	 * @param description the description
	 */
	protected IntegerOption(String description) {
		super(description);
	}

	@Override
	public String getStringValue() {
		return Integer.toString(value);
	}

	@Override
	public void fromJson(JsonElement e) {
		this.value = e.getAsInt();
	}

	@Override
	public JsonElement toJson() {
		return new JsonPrimitive(value);
	}
}
