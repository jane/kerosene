package pm.j4.kerosene.util.module.option;

import com.google.gson.JsonElement;

/**
 * The type List option.
 */
public class ListOption extends ConfigurationOption {
	/**
	 * Instantiates a new List option.
	 *
	 * @param description the description
	 */
	protected ListOption(String description) {
		super(description);
	}

	@Override
	public String getStringValue() {
		return "ListObject";
	}

	@Override
	public void fromJson(JsonElement e) {

	}

	@Override
	public JsonElement toJson() {
		return null;
	}
}
