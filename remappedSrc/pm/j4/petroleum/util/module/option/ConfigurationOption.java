package pm.j4.kerosene.util.module.option;

import com.google.gson.JsonElement;

/**
 * The type Configuration option.
 */
public abstract class ConfigurationOption {
	/**
	 * The Description.
	 */
	private final String description;

	/**
	 * Instantiates a new Configuration option.
	 *
	 * @param description the description
	 */
	protected ConfigurationOption(String description) {
		this.description = description;
	}

	/**
	 * Gets description.
	 *
	 * @return the description
	 */
	public final String getDescription() {
		return this.description;
	}

	/**
	 * Gets string value.
	 *
	 * @return the string value
	 */
	public abstract String getStringValue();

	/**
	 * From json.
	 *
	 * @param e the e
	 */
	public abstract void fromJson(JsonElement e);

	/**
	 * To json json element.
	 *
	 * @return the json element
	 */
	public abstract JsonElement toJson();

	/**
	 * Update.
	 */
	public void update() {

	}
}
