package pm.j4.kerosene.modules.list;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import jdk.nashorn.internal.runtime.options.Option;
import pm.j4.kerosene.PetroleumMod;
import pm.j4.kerosene.util.config.ConfigHolder;
import pm.j4.kerosene.util.config.ConfigManager;
import pm.j4.kerosene.util.module.ModuleBase;

/**
 * The type Mod list.
 */
public class ModList extends ModuleBase {
	/**
	 * Instantiates a new Mod list.
	 */
	public ModList() {
		super("petroleum.modlist",
				"petroleum.misc",
				true,
				true,
				true);
	}

	/**
	 * Gets active.
	 *
	 * @return the active
	 */
	public static List<ModuleBase> getActive() {
		List<ModuleBase> result = new ArrayList<>();
		Optional<ConfigHolder> config = ConfigManager.getConfig();
		if(config.isPresent()) {
			config.get().getEnabledModules().forEach((mod) -> {
				if (!mod.isHidden()) {
					result.add(mod);
				}
			});
		}
		return result;
	}
}
