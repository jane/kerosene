package pm.j4.kerosene.mixin;

import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.gui.screen.options.OptionsScreen;
import net.minecraft.client.gui.widget.ButtonWidget;
import net.minecraft.text.Text;
import net.minecraft.text.TranslatableText;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import pm.j4.kerosene.gui.POptionsScreen;

/**
 * The type Options menu mixin.
 */
@Mixin(OptionsScreen.class)
public class OptionsMenuMixin extends Screen {

	/**
	 * Instantiates a new Options menu mixin.
	 *
	 * @param title the title
	 */
	protected OptionsMenuMixin(Text title) {
		super(title);
	}

	/**
	 * Init.
	 *
	 * @param ci the ci
	 */
	@Inject(at = @At(value = "TAIL"),
			method = "init()V")
	protected void init(CallbackInfo ci) {
		this.addButton(new ButtonWidget(this.width / 2 - 75, this.height / 6 + 140, 150, 20, new TranslatableText("petroleum.options"), (buttonWidget) -> {
			assert this.client != null;
			this.client.openScreen(new POptionsScreen(this));
		}));
	}
}
