package pm.j4.kerosene.mixin;


import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.font.TextRenderer;
import net.minecraft.client.gui.DrawableHelper;
import net.minecraft.client.gui.hud.InGameHud;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.text.TranslatableText;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import pm.j4.kerosene.modules.list.ModList;
import pm.j4.kerosene.util.config.ConfigHolder;
import pm.j4.kerosene.util.config.ConfigManager;
import pm.j4.kerosene.util.module.ModuleBase;

/**
 * The type Mod list mixin.
 */
@Mixin(InGameHud.class)
public abstract class ModListMixin extends DrawableHelper {
	/**
	 * The Scaled height.
	 */
	@Shadow
	private int scaledHeight;
	/**
	 * The Client.
	 */
	private final MinecraftClient client;

	/**
	 * Gets font renderer.
	 *
	 * @return the font renderer
	 */
	@Shadow
	public abstract TextRenderer getFontRenderer();


	/**
	 * Instantiates a new Mod list mixin.
	 *
	 * @param client the client
	 */
	public ModListMixin(MinecraftClient client) {
		this.client = client;
	}

	/**
	 * Render.
	 *
	 * @param matrices  the matrices
	 * @param tickDelta the tick delta
	 * @param ci        the ci
	 */
	@Inject(method = "render(Lnet/minecraft/client/util/math/MatrixStack;F)V",
			at = @At("HEAD"))
	public void render(MatrixStack matrices, float tickDelta, CallbackInfo ci) {
		Optional<ConfigHolder> config = ConfigManager.getConfig();
		if (config.isPresent() &&
				config.get().isModuleEnabled("petroleum.modlist") &&
				!this.client.options.hudHidden &&
				!this.client.options.debugEnabled) {
			renderModuleList(matrices);
		}
	}

	/**
	 * Render module list.
	 *
	 * @param matrices the matrices
	 */
	private void renderModuleList(MatrixStack matrices) {
		List<ModuleBase> modules = ModList.getActive();
		List<TranslatableText> activeModuleList = modules.stream().map(module -> module.getReadableName()).collect(Collectors.toList());
		int fontHeight = this.getFontRenderer().fontHeight;
		int startHeight = this.scaledHeight - (activeModuleList.size() * (fontHeight + 4));
		for (int i = 0; i < activeModuleList.size(); i++) {
			this.getFontRenderer().drawWithShadow(matrices, activeModuleList.get(i), 10, 10 + (i * (fontHeight + 4)), -1);
		}
	}
}
