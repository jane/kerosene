package pm.j4.kerosene.mixin;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import net.minecraft.client.network.ClientPlayerEntity;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import pm.j4.kerosene.modules.bindings.ChatCommands;
import pm.j4.kerosene.util.data.ChatCommand;
import pm.j4.kerosene.util.event.Event;
import pm.j4.kerosene.util.event.EventContext;
import pm.j4.kerosene.util.event.EventManager;

@Mixin(ClientPlayerEntity.class)
public class ClientPlayerEntityMixin {
	@Inject(
			at = @At("HEAD"),
			method = "sendChatMessage",
			cancellable = true
	)
	public void onChatMessage(String message, CallbackInfo ci) {
		if(message.startsWith(".")) {
			String[] args = message.split(" ");
			String command = args[0].substring(1);
			Optional<ChatCommand> matchingCommands = ChatCommands.findCommand(command);

			List<String> newArgs = new ArrayList<>();
			for(int i = 1;i < args.length; i++) {
				newArgs.add(args[i]);
			}

			if (matchingCommands.isPresent()) {
				matchingCommands.get().execute(newArgs);
			}
			ci.cancel();
		}
	}

	@Inject(at =@At(value = "INVOKE",
	target = "Lnet/minecraft/client/network/AbstractClientPlayerEntity;tick()V",
	ordinal = 0),
	method = "tick()V")
	private void onTick(CallbackInfo ci) {
		EventManager.fire(Event.TICK, EventContext.NoEventContext);
	}
}
