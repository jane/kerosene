package pm.j4.kerosene.util.event;

import java.lang.annotation.Annotation;
import java.util.*;
import pm.j4.kerosene.util.config.Disables;
import pm.j4.kerosene.util.config.DisablesList;
import pm.j4.kerosene.util.module.ModuleBase;

public class EventManager {
	private static Map<Event, Set<ModuleBase>> listeners = new HashMap<>();

	public static void register(ModuleBase module) {
		try {
			Annotation moduleAnnotation = module.getClass().getAnnotation(Listener.class);
			if (moduleAnnotation != null) {
				Listener[] annotatedListeners = ((Listeners)moduleAnnotation).value();
				for (Listener listener : annotatedListeners) {
					Event event = listener.value();
					if (listeners.containsKey(event)) {
						Set<ModuleBase> modules = listeners.get(event);
						if(!modules.contains(module)) {
							modules.add(module);
						}
					}
					else {
						Set<ModuleBase> modules = Collections.singleton(module);
						listeners.put(event, modules);
					}
				}
			}
		}
		catch (Exception e) {
			System.out.println(e);
		}
	}

	public static void fire(Event event, EventContext context) {
		if (listeners.containsKey(event)) {
			listeners.get(event).forEach(
					listener -> {
						listener.receiveEvent(event, context);
					}
			);
		}
	}
}
