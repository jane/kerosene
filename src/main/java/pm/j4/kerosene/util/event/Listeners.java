package pm.j4.kerosene.util.event;

public @interface Listeners {
	public Listener[] value();
}
