package pm.j4.kerosene.util.config;

import java.util.ArrayList;
import java.util.List;
import pm.j4.kerosene.util.data.ModInfoProvider;
import pm.j4.kerosene.util.module.ModuleBase;

/**
 * The type Config.
 */
public abstract class Config {
	/**
	 * The Enabled modules.
	 */
	public List<String> enabledModules = new ArrayList<>();

	/**
	 * Is enabled boolean.
	 *
	 * @param mod the mod
	 * @return the boolean
	 */
	public boolean isEnabled(String mod) {
		return enabledModules.contains(mod);
	}


	/**
	 * Disable module.
	 *
	 * @param mod the mod
	 */
	public void disableModule(String mod) {
		if (isEnabled(mod) && ModInfoProvider.isActive(mod) && ModInfoProvider.getMod(mod).isPresent()) {
			ModuleBase moduleInfo = ModInfoProvider.getMod(mod).get();
			if (moduleInfo.isActivatable()) {
				enabledModules.remove(mod);
			}
		}
	}

	/**
	 * Toggle module.
	 *
	 * @param mod the mod
	 */
	public void toggleModule(String mod) {
		if (ModInfoProvider.isActive(mod) && ModInfoProvider.getMod(mod).isPresent()) {
			ModuleBase moduleInfo = ModInfoProvider.getMod(mod).get();
			if (moduleInfo.isActivatable()) {
				if (isEnabled(mod)) {
					enabledModules.remove(mod);
				} else {
					enabledModules.add(mod);
				}
			}
		}
	}
}
