package pm.j4.kerosene.util.config;

/**
 * The type Server config.
 */
public class ServerConfig extends Config {
	/**
	 * The Address.
	 */
	public String address = "";
}
