package pm.j4.kerosene.util.config;

import java.lang.annotation.Repeatable;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import pm.j4.kerosene.util.module.ModuleBase;

@Retention(RetentionPolicy.RUNTIME)
@Repeatable(DisablesList.class)
public @interface Disables {
	public String value();
}
