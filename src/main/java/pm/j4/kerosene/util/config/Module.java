package pm.j4.kerosene.util.config;

import java.lang.annotation.Repeatable;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.ArrayList;
import java.util.List;
import pm.j4.kerosene.util.module.ModuleBase;

@Retention(RetentionPolicy.RUNTIME)
@Repeatable(Modules.class)
public @interface Module {
	public Class<? extends ModuleBase> value();
}