package pm.j4.kerosene.util.config;

import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;
import net.minecraft.client.options.KeyBinding;
import net.minecraft.client.util.InputUtil;
import pm.j4.kerosene.modules.bindings.BindingInfo;
import pm.j4.kerosene.modules.bindings.BindingManager;
import pm.j4.kerosene.util.data.ModInfoProvider;
import pm.j4.kerosene.util.data.OptionSerializiable;
import pm.j4.kerosene.util.module.ModuleBase;
import pm.j4.kerosene.util.module.ModuleFunction;
import pm.j4.kerosene.util.module.option.ConfigurationOption;

/**
 * The type Global config.
 */
public class GlobalConfig extends Config {
	/**
	 * The Bindings.
	 */
	public final Map<KeyBinding, ModuleFunction> bindings = new HashMap<>();

	/**
	 * Is bound boolean.
	 *
	 * @param func the func
	 * @return the boolean
	 */
	public boolean isBound(ModuleFunction func) {
		AtomicBoolean found = new AtomicBoolean(false);
		bindings.forEach((key, binding) -> {
			if (binding.equals(func)) {
				found.set(true);
			}
		});
		return found.get();
	}

	/**
	 * Sets binding.
	 *
	 * @param bind the bind
	 * @param func the func
	 */
	public void setBinding(KeyBinding bind, ModuleFunction func) {
		AtomicReference<KeyBinding> match = new AtomicReference<>();
		if (bindings.containsValue(func)) {
			bindings.forEach((key, binding) -> {
				if (binding.equals(func)) {
					BindingManager.removeBind(key);
					match.set(key);
				}
			});
		}

		if (match.get() != null) {
			bindings.remove(match.get());
		}

		if (ModInfoProvider.isActive(func.getParent().getModuleName())) {
			BindingManager.addBind(bind, func);
			func.getParent().setBinding(bind, func);
			bindings.put(bind, func);
		}
	}

	/**
	 * Convert binding.
	 *
	 * @param info the info
	 */
	private void convertBinding(BindingInfo info) {
		Optional<ModuleBase> match = ModInfoProvider.getMod(info.attachedModuleName);
		match.ifPresent(moduleBase -> {
			List<ModuleFunction> foundBinds = moduleBase.matchBinding(info.attachedFunctionName);
			if (foundBinds.size() > 0) {
				setBinding(reconstructBinding(info), foundBinds.get(0));
			}
		});
	}

	/**
	 * Reconstruct binding key binding.
	 *
	 * @param info the info
	 * @return the key binding
	 */
	public static KeyBinding reconstructBinding(BindingInfo info) {
		return new KeyBinding(
				info.translationKey,
				info.type,
				info.key,
				info.category
		);
	}

	/**
	 * Extract binding info.
	 *
	 * @param b the b
	 * @param f the f
	 * @return the binding info
	 */
	public static BindingInfo extractBinding(KeyBinding b, ModuleFunction f) {
		BindingInfo res = new BindingInfo();
		res.attachedModuleName = f.getParent().getModuleName();
		res.attachedFunctionName = f.getFunctionName();

		res.translationKey = b.getTranslationKey();
		InputUtil.Key k = b.getDefaultKey();
		res.type = k.getCategory();
		res.key = k.getCode();
		res.category = b.getCategory();

		return res;
	}

	/**
	 * Serialize bindings list.
	 *
	 * @return the list
	 */
	public List<BindingInfo> serializeBindings() {
		List<BindingInfo> b = new ArrayList<>();
		bindings.forEach((k, f) -> b.add(extractBinding(k, f)));
		return b;
	}

	/**
	 * Deserialize bindings.
	 *
	 * @param info the info
	 */
	public void deserializeBindings(List<BindingInfo> info) {
		info.forEach(this::convertBinding);
	}

	/**
	 * Serialize module configuration list.
	 *
	 * @param module the module
	 * @return the list
	 */
	public static Map<String, OptionSerializiable> serializeModuleConfiguration(ModuleBase module) {
		Map<String, OptionSerializiable> opts = new HashMap<>();
		Map<String, ConfigurationOption> configuration = module.getModuleConfiguration();
		configuration.forEach((key, value) -> opts.put(key, new OptionSerializiable(key, value.toJson())));
		return opts;
	}

	/**
	 * Deserialize module configuration.
	 *
	 * @param opts   the opts
	 * @param module the module
	 */
	public void deserializeModuleConfiguration(List<OptionSerializiable> opts, ModuleBase module) {
		opts.forEach(option -> {
			if (module.hasOption(option.key)) {
				module.setConfigOption(option.key, option.value);
			}
		});
	}
}

