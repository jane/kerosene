package pm.j4.kerosene.util.data;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import net.minecraft.client.MinecraftClient;
import net.minecraft.server.integrated.IntegratedServer;
import pm.j4.kerosene.util.module.ModuleBase;

/**
 * The type Mod info provider.
 */
//TODO refactor into multiple data holders
public class ModInfoProvider {
	/**
	 * The constant client.
	 */
	public static MinecraftClient client;
	/**
	 * The constant activeMods.
	 */
	private static final List<ModuleBase> registeredMods = new ArrayList<>();

	/**
	 * Register mod.
	 *
	 * @param mod the mod
	 * @throws IllegalAccessException the illegal access exception
	 * @throws InstantiationException the instantiation exception
	 */
	public static void registerMod(Class<? extends ModuleBase> mod) throws IllegalAccessException, InstantiationException {
		ModuleBase base = mod.newInstance();
		if(!registeredMods.contains(base)) {
			registeredMods.add(base);
		}
	}

	/**
	 * Is active boolean.
	 *
	 * @param modName the mod name
	 * @return the boolean
	 */
	public static boolean isActive(String modName) {
		return registeredMods.stream().anyMatch(mod -> mod.getModuleName().equals(modName));
	}

	/**
	 * Gets mod.
	 *
	 * @param modName the mod name
	 * @return the mod
	 */
	public static Optional<ModuleBase> getMod(String modName) {
		return registeredMods.stream().filter(mod -> mod.getModuleName().equals(modName)).findFirst();
	}

	/**
	 * Gets active mods.
	 *
	 * @return the active mods
	 */
	public static List<ModuleBase> getRegisteredMods() {
		return registeredMods;
	}

	/**
	 * Gets server address.
	 *
	 * @return the server address
	 */
	public static String getServerAddress() {
		if (client != null && client.getServer() != null) {
			IntegratedServer server = client.getServer();
			if (!server.isRemote()) {
				return "localhost";
			}
			if (server.isRemote() && !server.getServerIp().isEmpty()) {
				return server.getServerIp();
			}
		}
		return null;
	}
}
