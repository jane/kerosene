package pm.j4.kerosene.util.data;

import java.util.List;

public abstract class ChatCommand {
	public ChatCommand(String name) {
		this.name = name;
	}

	private final String name;

	public String getName() {
		return name;
	}

	public abstract void execute(List<String> args);
}
