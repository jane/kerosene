package pm.j4.kerosene.util.data;

import com.google.gson.JsonElement;

/**
 * The type Option serializiable.
 */
public class OptionSerializiable {
	/**
	 * Instantiates a new Option serializiable.
	 *
	 * @param key   the key
	 * @param value the value
	 */
	public OptionSerializiable(String key, JsonElement value) {
		this.value = value;
		this.key = key;
	}

	/**
	 * The Value.
	 */
	public final JsonElement value;
	/**
	 * The Key.
	 */
	public final String key;
}
