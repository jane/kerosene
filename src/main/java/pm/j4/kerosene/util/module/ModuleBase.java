package pm.j4.kerosene.util.module;

import com.google.gson.JsonElement;
import java.lang.annotation.Annotation;
import java.util.*;
import java.util.stream.Collectors;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.options.KeyBinding;
import net.minecraft.text.TranslatableText;
import pm.j4.kerosene.gui.PModuleConfigEntry;
import pm.j4.kerosene.gui.PModuleConfigPane;
import pm.j4.kerosene.modules.bindings.BindingManager;
import pm.j4.kerosene.modules.bindings.ChatCommands;
import pm.j4.kerosene.util.config.ConfigHolder;
import pm.j4.kerosene.util.config.ConfigManager;
import pm.j4.kerosene.util.config.Disables;
import pm.j4.kerosene.util.config.DisablesList;
import pm.j4.kerosene.util.data.ChatCommand;
import pm.j4.kerosene.util.event.Event;
import pm.j4.kerosene.util.event.EventContext;
import pm.j4.kerosene.util.event.EventManager;
import pm.j4.kerosene.util.module.option.ConfigurationOption;

/**
 * The Basis for all mods, used so that modules all have a common activation point and settings.
 */
public abstract class ModuleBase {

	/**
	 * Instantiates a new Module base.
	 * Parameters should be constant across restarts.
	 *
	 * @param parent        the parent
	 * @param name          The name of the module
	 * @param category      the category
	 * @param activatable   Whether a module can be activated, or if it will remain in the state it was upon startup
	 * @param hidden        Whether the module will show up in @link pm.j4.petroleum.modules.menu.ModMenu or the active module list
	 * @param hasConfigMenu whether a button in the configuration menu will show
	 */
	public ModuleBase(String parent, String name, String category, boolean activatable, boolean hidden, boolean hasConfigMenu) {
		this.parent = parent;
		this.moduleName = name;
		this.category = category;
		this.readableName = new TranslatableText(name);
		this.activatable = activatable;
		this.hidden = hidden;
		this.hasConfigMenu = hasConfigMenu;
		this.moduleOptions = this.convertDefaultConfig();
	}

	/**
	 * Init.
	 */
	public void init() {
		BindingManager.registerBindings(this);
		EventManager.register(this);
		ChatCommands.registerCommands(this.getChatCommands());
	}

	/**
	 * Activate. Should be overridden.
	 *
	 * @param client the client
	 */
	public void activate(MinecraftClient client) {
		this.toggle();
	}

	/**
	 * Toggle mod.
	 */
	public void toggle() {
		Optional<ConfigHolder> config = ConfigManager.getConfig(this.getParent());
		try {
			Annotation moduleAnnotation = this.getClass().getAnnotation(DisablesList.class);
			if (moduleAnnotation != null) {
				Disables[] modules = ((DisablesList)moduleAnnotation).value();
				System.out.println(modules);
				for (int i = 0; i < modules.length; i++) {
					System.out.println("disabling " + modules[i].value() + " as a requirement for " + this.getModuleName());
					int index = i;
					config.ifPresent(configHolder -> configHolder.disableModule(modules[index].value()));
				}
			}
		}
		catch (Exception e) {
			System.out.println(e);
		}
		config.ifPresent(configHolder -> configHolder.toggleModule(this.moduleName));
	}

	public boolean initialized;


	/**
	 * The Parent.
	 */
	private final String parent;

	/**
	 * Gets parent.
	 *
	 * @return the parent
	 */
	public String getParent() {
		return this.parent;
	}

	/**
	 * The Module's name.
	 */
	private final String moduleName;

	/**
	 * Gets module name.
	 *
	 * @return the module name
	 */
	public String getModuleName() {
		return this.moduleName;
	}

	/**
	 * The Category.
	 */
	private final String category;

	/**
	 * Gets category.
	 *
	 * @return the category
	 */
	public String getCategory() {
		return this.category;
	}

	/**
	 * The Readable name.
	 */
	private final TranslatableText readableName;

	/**
	 * Gets readable name.
	 *
	 * @return the readable name
	 */
	public TranslatableText getReadableName() {
		return this.readableName;
	}

	/**
	 * The Activatable.
	 */
	private final boolean activatable;

	/**
	 * Is activatable boolean.
	 *
	 * @return the boolean
	 */
	public boolean isActivatable() {
		return activatable;
	}

	/**
	 * The Hidden.
	 */
	private final boolean hidden;

	/**
	 * Is hidden boolean.
	 *
	 * @return the boolean
	 */
	public boolean isHidden() {
		return hidden;
	}


	/**
	 * The Has config menu.
	 */
	private final boolean hasConfigMenu;

	/**
	 * Configurable boolean.
	 *
	 * @return the boolean
	 */
	public boolean configurable() {
		return hasConfigMenu;
	}

	/**
	 * The Module options.
	 */
	private final Map<String, ConfigurationOption> moduleOptions;

	/**
	 * Gets module configuration.
	 *
	 * @return the module configuration
	 */
	public Map<String, ConfigurationOption> getModuleConfiguration() {
		return moduleOptions;
	}

	/**
	 * Sets config option.
	 * This will fail if the option is not already present in a module.
	 * <p>
	 *
	 * @param key   the key
	 * @param value the value
	 */
	public void setConfigOption(String key, JsonElement value) {
		if (moduleOptions.containsKey(key)) {
			moduleOptions.get(key).fromJson(value);
		}
	}

	/**
	 * Update config option.
	 *
	 * @param key    the key
	 * @param option the option
	 */
	public void updateConfigOption(String key, ConfigurationOption option) {
		System.out.println("update config option" + key + option.getStringValue());
		System.out.println(moduleOptions.keySet());
		if(moduleOptions.containsKey(key)) {
			System.out.println("matched");
			moduleOptions.replace(key, option);
		}
	}

	/**
	 * Has option boolean.
	 *
	 * @param key the key
	 * @return the boolean
	 */
	public boolean hasOption(String key) {
		return moduleOptions.containsKey(key);
	}

	/**
	 * Gets default config.
	 *
	 * @return the default config
	 */
	protected List<ConfigurationOption> getDefaultConfig() {
		return new ArrayList<>();
	}

	/**
	 * Convert default config map.
	 *
	 * @return the map
	 */
	private Map<String, ConfigurationOption> convertDefaultConfig() {
		List<ConfigurationOption> options = this.getDefaultConfig();
		Map<String, ConfigurationOption> mapped = new HashMap<>();
		options.forEach((option) -> mapped.put(option.getConfigKey(), option));
		return mapped;
	}

	/**
	 * Gets config option.
	 *
	 * @param key the key
	 * @return the config option
	 */
	public Optional<ConfigurationOption> getConfigOption(String key) {
		if (moduleOptions.containsKey(key)) {
			return Optional.of(moduleOptions.get(key));
		}
		return Optional.empty();
	}

	protected List<ChatCommand> getChatCommands() {
		return new ArrayList<>();
	};

	private Map<KeyBinding, ModuleFunction> attachedBinds = new HashMap<>();

	public Map<KeyBinding, ModuleFunction> getDefaultBindings() {
		return new HashMap<>();
	}

	public List<ModuleFunction> matchBinding(String name) {
		List<ModuleFunction> result = attachedBinds.entrySet().stream()
		.filter(entry -> entry.getValue().getFunctionName().equals(name))
				.map(entry -> entry.getValue())
				.collect(Collectors.toList());
		return result;
	}

	public void setBinding(KeyBinding bind, ModuleFunction function) {
		if (attachedBinds.containsKey(bind)) {
			attachedBinds.replace(bind, function);
		}
		else {
			attachedBinds.put(bind, function);
		}
	}

	/**
	 * Gets config entries.
	 *
	 * @param sourcePane the source pane
	 * @return the config entries
	 */
	public List<PModuleConfigEntry> getConfigEntries(PModuleConfigPane sourcePane) {
		List<PModuleConfigEntry> entries = new ArrayList<>();
		this.getModuleConfiguration().forEach((name, option) -> entries.add(new PModuleConfigEntry(option, new TranslatableText(name), sourcePane)));
		return entries;
	}

	public void receiveEvent(Event event, EventContext context) {}

	// TODO evaluate if necessary
	/**
	 * Equals boolean.
	 *
	 * @param other the other
	 * @return the boolean
	 */
	public boolean equals(ModuleBase other) {
		return Objects.equals(this.moduleName, other.getModuleName());
	}
}
