package pm.j4.kerosene.util.module;

import net.minecraft.client.MinecraftClient;

public class ModuleFunction {
	public ModuleFunction(String name, ModuleBase parent) {
		this.name = name;
		this.parent = parent;
	}
	private final ModuleBase parent;
	private final String name;

	public ModuleBase getParent() {
		return parent;
	}

	public String getFunctionName() {
		return name;
	}

	public void activate(MinecraftClient client) {
		this.parent.activate(client);
	}
}
