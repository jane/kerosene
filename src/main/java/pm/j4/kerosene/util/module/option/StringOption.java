package pm.j4.kerosene.util.module.option;

import com.google.gson.JsonElement;
import com.google.gson.JsonPrimitive;
import pm.j4.kerosene.util.module.ModuleBase;

/**
 * The type String value.
 */
public class StringOption extends ConfigurationOption {

	/**
	 * The Value.
	 */
	private String value;

	/**
	 * Instantiates a new Configuration option.
	 *
	 * @param key         the key
	 * @param description the description
	 * @param parent      the parent
	 */
	public StringOption(String key, String description, ModuleBase parent) {
		super(key, description, parent);
	}


	public String getStringValue() {
		return value;
	}

	@Override
	public void fromJson(JsonElement e) {
		this.value = e.getAsString();
	}

	@Override
	public JsonElement toJson() {
		return new JsonPrimitive(value);
	}
}
