package pm.j4.kerosene.util.module.option;

import com.google.gson.JsonElement;
import pm.j4.kerosene.util.module.ModuleBase;

/**
 * The type List option.
 */
public class ListOption extends ConfigurationOption {


	/**
	 * Instantiates a new Configuration option.
	 *
	 * @param key         the key
	 * @param description the description
	 * @param parent      the parent
	 */
	public ListOption(String key, String description, ModuleBase parent) {
		super(key, description, parent);
	}

	@Override
	public String getStringValue() {
		return "ListObject";
	}

	@Override
	public void fromJson(JsonElement e) {

	}

	@Override
	public JsonElement toJson() {
		return null;
	}
}
