package pm.j4.kerosene.util.module.option;

import com.google.gson.JsonElement;
import net.minecraft.client.options.KeyBinding;
import pm.j4.kerosene.modules.bindings.BindingInfo;
import pm.j4.kerosene.util.config.ConfigManager;
import pm.j4.kerosene.util.config.GlobalConfig;
import pm.j4.kerosene.util.module.ModuleBase;
import pm.j4.kerosene.util.module.ModuleFunction;

/**
 * The type Keybind value.
 */
public class KeybindOption extends ConfigurationOption {

	/**
	 * The Value.
	 */
	private KeyBinding value;
	/**
	 * The Converted value.
	 */
	private BindingInfo convertedValue;

	/**
	 * Instantiates a new Configuration option.
	 *
	 * @param key         the key
	 * @param description the description
	 * @param parent      the parent
	 */
	public KeybindOption(String key, String description, ModuleBase parent) {
		super(key, description, parent);
	}

	/**
	 * Gets translation key.
	 *
	 * @return the translation key
	 */
	public String getTranslationKey() {
		return value.getTranslationKey();
	}

	@Override
	public String getStringValue() {
		return value.getDefaultKey().getLocalizedText().getString();
	}

	public BindingInfo getBindingInfo() {
		return convertedValue;
	}

	@Override
	public void fromJson(JsonElement e) {
		BindingInfo bindingInfo = ConfigManager.deserializeElement(e, BindingInfo.class);
		this.convertedValue = bindingInfo;
		this.value = GlobalConfig.reconstructBinding(bindingInfo);
	}

	@Override
	public JsonElement toJson() {
		return null;
	}

	/**
	 * From keybind.
	 *
	 * @param bind the bind
	 * @param base the base
	 */
	public void fromKeybind(KeyBinding bind, ModuleFunction base) {
		this.value = bind;
		this.convertedValue = GlobalConfig.extractBinding(bind, base);
	}
}
