package pm.j4.kerosene.util.module.option;

import com.google.gson.JsonElement;
import com.google.gson.JsonPrimitive;
import pm.j4.kerosene.util.module.ModuleBase;

/**
 * The type Boolean value.
 */
public class BooleanOption extends ConfigurationOption {
	/**
	 * The Value.
	 */
	private boolean value;

	/**
	 * Instantiates a new Configuration option.
	 *
	 * @param key         the key
	 * @param description the description
	 * @param parent      the parent
	 */
	public BooleanOption(String key, String description, ModuleBase parent) {
		super(key, description, parent);
	}

	/**
	 * Gets value.
	 *
	 * @return the value
	 */
	public boolean getValue() {
		return value;
	}

	/**
	 * Sets value.
	 *
	 * @param value the value
	 */
	public void setValue(boolean value) { this.value = value; }

	@Override
	public String getStringValue() {
		return Boolean.toString(value);
	}

	@Override
	public void fromJson(JsonElement e) {
		this.value = e.getAsBoolean();
	}

	@Override
	public JsonElement toJson() {
		return new JsonPrimitive(value);
	}
}
