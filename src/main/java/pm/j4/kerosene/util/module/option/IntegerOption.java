package pm.j4.kerosene.util.module.option;

import com.google.gson.JsonElement;
import com.google.gson.JsonPrimitive;
import pm.j4.kerosene.util.module.ModuleBase;

/**
 * The type Integer value.
 */
public class IntegerOption extends ConfigurationOption {
	/**
	 * The Value.
	 */
	private int value;

	/**
	 * Instantiates a new Configuration option.
	 *
	 * @param key         the key
	 * @param description the description
	 * @param parent      the parent
	 */
	public IntegerOption(String key, String description, ModuleBase parent) {
		super(key, description, parent);
	}


	@Override
	public String getStringValue() {
		return Integer.toString(value);
	}

	@Override
	public void fromJson(JsonElement e) {
		this.value = e.getAsInt();
	}

	@Override
	public JsonElement toJson() {
		return new JsonPrimitive(value);
	}
}
