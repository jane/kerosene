package pm.j4.kerosene.util.module.option;

import com.google.gson.JsonElement;
import pm.j4.kerosene.util.module.ModuleBase;

/**
 * The type Configuration option.
 */
public abstract class ConfigurationOption {
	/**
	 * The Description.
	 */
	private final String description;
	/**
	 * The Key.
	 */
	private final String key;
	/**
	 * The Parent.
	 */
	private final ModuleBase parent;

	/**
	 * Instantiates a new Configuration option.
	 *
	 * @param key         the key
	 * @param description the description
	 * @param parent      the parent
	 */
	public ConfigurationOption(String key, String description, ModuleBase parent) {
		this.description = description;
		this.key = key;
		this.parent = parent;
	}

	/**
	 * Gets description.
	 *
	 * @return the description
	 */
	public final String getDescription() {
		return this.description;
	}

	/**
	 * Gets config key.
	 *
	 * @return the config key
	 */
	public final String getConfigKey() { return key; }

	/**
	 * Gets parent.
	 *
	 * @return the parent
	 */
	public final ModuleBase getParent() { return parent; }

	/**
	 * Gets string value.
	 *
	 * @return the string value
	 */
	public abstract String getStringValue();

	/**
	 * From json.
	 *
	 * @param e the e
	 */
	public abstract void fromJson(JsonElement e);

	/**
	 * To json json element.
	 *
	 * @return the json element
	 */
	public abstract JsonElement toJson();

}
