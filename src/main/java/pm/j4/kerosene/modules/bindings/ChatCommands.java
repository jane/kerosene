package pm.j4.kerosene.modules.bindings;

import java.util.*;
import java.util.stream.Collectors;
import pm.j4.kerosene.util.data.ChatCommand;
import pm.j4.kerosene.util.module.ModuleBase;

public class ChatCommands extends ModuleBase {
	/**
	 * Instantiates a new Module base.
	 * Parameters should be constant across restarts.
	 */
	public ChatCommands() {
		super("kerosene",
				"kerosene.chatcommands",
				"kerosene.misc",
				false,
				true,
				false);
	}

	private static Map<String, ChatCommand> commands = new HashMap<>();

	public static void registerCommands(List<ChatCommand> commandList) {
		for(ChatCommand command : commandList) {
			if(commands.containsKey(command.getName())) {
				return;
			}
			System.out.println("REGISTERING COMMAND " + command.getName());
			commands.put(command.getName(), command);
		}
	}

	public static Optional<ChatCommand> findCommand(String text) {
		if (commands.containsKey(text)) {
			return Optional.of(commands.get(text));
		}
		else {
			System.out.println("COULD NOT FIND CHAT COMMAND WITH NAME " + text);
			return Optional.empty();
		}
	}
}
