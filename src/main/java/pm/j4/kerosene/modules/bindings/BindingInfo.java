package pm.j4.kerosene.modules.bindings;

import net.minecraft.client.util.InputUtil;

/**
 * The type Binding info.
 */
public class BindingInfo {
	/**
	 * The Translation key.
	 */
	public String translationKey;
	/**
	 * The Type.
	 */
	public InputUtil.Type type;
	/**
	 * The Key.
	 */
	public int key;
	/**
	 * The Category.
	 */
	public String category;

	/**
	 * The Attached function id.
	 */
	public String attachedModuleName;

	public String attachedFunctionName;
}
