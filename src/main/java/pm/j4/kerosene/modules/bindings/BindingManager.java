package pm.j4.kerosene.modules.bindings;

import java.util.*;
import net.minecraft.client.options.KeyBinding;
import net.minecraft.text.TranslatableText;
import pm.j4.kerosene.gui.PModuleConfigEntry;
import pm.j4.kerosene.gui.PModuleConfigPane;
import pm.j4.kerosene.util.config.ConfigHolder;
import pm.j4.kerosene.util.config.ConfigManager;
import pm.j4.kerosene.util.module.ModuleBase;
import pm.j4.kerosene.util.module.ModuleFunction;
import pm.j4.kerosene.util.module.option.KeybindOption;

/**
 * The type Binding manager.
 */
public class BindingManager extends ModuleBase {
	/**
	 * Instantiates a new Module base.
	 * Parameters should be constant across restarts.
	 */
	public BindingManager() {
		super("kerosene",
				"kerosene.bindings",
				"kerosene.misc",
				false,
				true,
				true);
	}

	@Override
	public void init() {
		super.init();
	}

	@Override
	public List<PModuleConfigEntry> getConfigEntries(PModuleConfigPane sourcePane) {

		//TODO multiple binds per module
		// thoughts: have modules include a list of module triggers/functions
		// which replace the ModuleBase in bindings?
		List<PModuleConfigEntry> entries = new ArrayList<>();

		Map<KeybindOption, ModuleFunction> mapped = new HashMap<>();
		Map<String, ConfigHolder> configs = ConfigManager.getAllConfigs();
		configs.forEach((modName, config) -> {
			if (config.globalConfig != null) {
				Map<KeyBinding, ModuleFunction> binds = config.globalConfig.bindings;
				binds.forEach((key, func) -> {
					ModuleBase parent = func.getParent();
					KeybindOption option = new KeybindOption(parent.getModuleName() + " " + parent.getCategory(), parent.getModuleName() + " " + parent.getCategory(), parent);
					option.fromKeybind(key, func);
					mapped.put(option, func);
				});
			}
		});
		mapped.forEach((configEntry, func) -> {
			ModuleBase module = func.getParent();
			PModuleConfigEntry entry = new PModuleConfigEntry(configEntry, new TranslatableText(module.getModuleName()), sourcePane);
			entries.add(entry);
		});
		return entries;
	}

	/**
	 * Register bindings.
	 */
	public static void registerBindings(ModuleBase module) {
		System.out.println("registering bindings for " + module.getModuleName());
		Optional<ConfigHolder> config = ConfigManager.getConfig(module.getParent());
		Map<KeyBinding, ModuleFunction> defaults = module.getDefaultBindings();
		config.ifPresent((holder) -> {
			System.out.println("config present");
			defaults.forEach((bind, func) -> {
				System.out.println("default value:" + bind.getTranslationKey() + " bound to " + func.getFunctionName());
				if(!holder.globalConfig.isBound(func)) {
					System.out.println("no value set, using default");
					holder.globalConfig.setBinding(bind, func);
				}
			});
		});
	}

	/**
	 * The constant registeredBinds.
	 */
	private static final Map<KeyBinding, ModuleFunction> registeredBinds = new HashMap<>();

	/**
	 * Add bind.
	 *
	 * @param bind the bind
	 * @param function the function
	 */
	public static void addBind(KeyBinding bind, ModuleFunction function) {
		registeredBinds.put(bind, function);
	}

	/**
	 * Remove bind.
	 *
	 * @param b the b
	 */
	public static void removeBind(KeyBinding b) {
		registeredBinds.remove(b);
	}

	/**
	 * Gets active keybinds.
	 *
	 * @return the active keybinds
	 */
	public static Map<KeyBinding, ModuleFunction> getActiveKeybinds() {
		return registeredBinds;
	}
}
