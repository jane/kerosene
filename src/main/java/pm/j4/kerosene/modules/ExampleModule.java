package pm.j4.kerosene.modules;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.options.KeyBinding;
import net.minecraft.client.util.InputUtil;
import org.lwjgl.glfw.GLFW;
import pm.j4.kerosene.util.data.ChatCommand;
import pm.j4.kerosene.util.module.ModuleBase;
import pm.j4.kerosene.util.module.ModuleFunction;
import pm.j4.kerosene.util.module.option.BooleanOption;
import pm.j4.kerosene.util.module.option.ConfigurationOption;

/**
 * The type Example module.
 */
public class ExampleModule extends ModuleBase {
	/**
	 * example mod
	 */
	public ExampleModule() {
		super("kerosene",
				"kerosene.example",
				"kerosene.misc",
				true,
				false,
				true);
	}

	@Override
	protected List<ChatCommand> getChatCommands() {
		List<ChatCommand> commands = new ArrayList<>();
		ChatCommand example = new ChatCommand("test") {
			@Override
			public void execute(List<String> args) {
				System.out.println("EXECUTED TEST COMMAND");
				args.forEach(
						(arg) -> {
							System.out.println("args: " + arg);
						}
				);
			}
		};

		commands.add(example);

		return commands;
	}

	@Override
	public List<ConfigurationOption> getDefaultConfig() {
		List<ConfigurationOption> options = new ArrayList<>();
		options.add(new BooleanOption("kerosene.example.b_one","example", this));
		options.add(new BooleanOption("kerosene.example.b_two","example", this));
		return options;
	}

	@Override
	public Map<KeyBinding, ModuleFunction> getDefaultBindings() {
		Map<KeyBinding, ModuleFunction> binds = new HashMap<>();

		KeyBinding openMenu = new KeyBinding(
				"key.kerosene.example",
				InputUtil.Type.KEYSYM,
				GLFW.GLFW_KEY_P,
				"category.kerosene"
		);
		ModuleFunction exampleFunc = new ModuleFunction("kerosene.example.test", this) {
			@Override
			public void activate(MinecraftClient client) {
				System.out.println("example function activated");
			}
		};
		binds.put(openMenu, exampleFunc);

		return binds;
	}

	@Override
	public void activate(MinecraftClient client) {
		System.out.println("Example Mod Keybind Activate");
	}
}
