package pm.j4.kerosene.gui;

import com.mojang.blaze3d.systems.RenderSystem;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.gui.widget.AlwaysSelectedEntryListWidget;
import net.minecraft.client.render.BufferBuilder;
import net.minecraft.client.render.Tessellator;
import net.minecraft.client.render.VertexFormats;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.Matrix4f;
import pm.j4.kerosene.mixin.EntryListWidgetAccessor;
import pm.j4.kerosene.util.module.ModuleBase;

/**
 * The type P module configuration widget.
 */
@SuppressWarnings("deprecation")
public class PModuleConfigurationWidget extends AlwaysSelectedEntryListWidget<POptionEntry> {
	/**
	 * The Parent.
	 */
	private final POptionsScreen parent;
	/**
	 * The Module id.
	 */
	private String moduleId = null;
	/**
	 * The Mods.
	 */
	private List<ModuleBase> mods;
	/**
	 * The Extra mods.
	 */
	private final Set<ModuleBase> extraMods = new HashSet<>();
	/**
	 * The Scrolling.
	 */
	private boolean scrolling = false;

	/**
	 * Instantiates a new P module configuration widget.
	 *
	 * @param client      the client
	 * @param width       the width
	 * @param height      the height
	 * @param y1          the y 1
	 * @param y2          the y 2
	 * @param entryHeight the entry height
	 * @param list        the list
	 * @param parent      the parent
	 */
	public PModuleConfigurationWidget(MinecraftClient client, int width, int height, int y1, int y2, int entryHeight, PModuleConfigurationWidget list, POptionsScreen parent) {
		super(client, width, height, y1, y2, entryHeight);
		this.parent = parent;
		if (list != null) {
			mods = list.mods;
		}
		setScrollAmount(parent.getScrollPercent() * Math.max(0, this.getMaxPosition() - (this.bottom - this.top - 4)));
	}

	@Override
	public void setScrollAmount(double amount) {
		super.setScrollAmount(amount);
		int denominator = Math.max(0, this.getMaxPosition() - (this.bottom - this.top - 4));
		if (denominator <= 0) {
			parent.updateScrollPercent(0);
		} else {
			parent.updateScrollPercent(getScrollAmount() / Math.max(0, this.getMaxPosition() - (this.bottom - this.top - 4)));
		}
	}

	@Override
	protected boolean isFocused() {
		return parent.getFocused() == this;
	}

	/**
	 * Select.
	 *
	 * @param entry the entry
	 */
	public void select(POptionEntry entry) {
		this.setSelected(entry);
	}

	@Override
	public void setSelected(POptionEntry entry) {
		super.setSelected(entry);
		moduleId = entry.getModId();
		parent.updateSelected(entry);
	}

	@Override
	protected boolean isSelectedEntry(int index) {
		return super.isSelectedEntry(index);
	}

	@Override
	public int addEntry(POptionEntry entry) {
		if (extraMods.contains(entry.module)) {
			return 0;
		}
		extraMods.add(entry.module);
		int i = super.addEntry(entry);
		if (entry.getModId().equals(moduleId)) {
			setSelected(entry);
		}
		return i;
	}

	@Override
	protected boolean removeEntry(POptionEntry entry) {
		extraMods.remove(entry.module);
		return super.removeEntry(entry);
	}

	@Override
	protected POptionEntry remove(int index) {
		extraMods.remove(getEntry(index).module);
		return super.remove(index);
	}

	@Override
	protected void renderList(MatrixStack matrices, int x, int y, int mouseX, int mouseY, float delta) {
		int itemCount = this.getEntryCount();
		Tessellator t_1 = Tessellator.getInstance();
		BufferBuilder buffer = t_1.getBuffer();

		for (int index = 0; index < itemCount; ++index) {
			int entryTop = this.getRowTop(index);
			int entryBottom = this.getRowTop(index) + this.itemHeight;
			if (entryBottom >= this.top && entryTop <= this.bottom) {
				int entryHeight = this.itemHeight - 4;
				POptionEntry entry = this.getEntry(index);
				int rowWidth = this.getRowWidth();
				int entryLeft;
				if (((EntryListWidgetAccessor) this).isRenderSelection() && this.isSelectedEntry(index)) {
					entryLeft = getRowLeft() - 2 + entry.getXOffset();
					int selectionRight = x + rowWidth + 2;
					RenderSystem.disableTexture();
					float brightness = this.isFocused() ? 1.0F : 0.5F;
					RenderSystem.color4f(brightness, brightness, brightness, 1.0F);
					Matrix4f matrix = matrices.peek().getModel();
					buffer.begin(7, VertexFormats.POSITION);
					buffer.vertex(matrix, entryLeft, entryTop + entryHeight + 2, 0.0F).next();
					buffer.vertex(matrix, selectionRight, entryTop + entryHeight + 2, 0.0F).next();
					buffer.vertex(matrix, selectionRight, entryTop - 2, 0.0F).next();
					buffer.vertex(matrix, entryLeft, entryTop - 2, 0.0F).next();
					t_1.draw();
					RenderSystem.color4f(0.0F, 0.0F, 0.0F, 1.0F);
					buffer.begin(7, VertexFormats.POSITION);
					buffer.vertex(matrix, entryLeft + 1, entryTop + entryHeight + 1, 0.0F).next();
					buffer.vertex(matrix, selectionRight, entryTop + entryHeight + 1, 0.0F).next();
					buffer.vertex(matrix, selectionRight, entryTop - 1, 0.0F).next();
					buffer.vertex(matrix, entryLeft + 1, entryTop - 1, 0.0F).next();
					t_1.draw();
					RenderSystem.enableTexture();
				}

				entryLeft = this.getRowLeft();
				entry.render(matrices,
						index,
						entryTop,
						entryLeft,
						rowWidth,
						entryHeight,
						mouseX,
						mouseY,
						this.isMouseOver(mouseX, mouseY) && Objects.equals(this.getEntryAtPos(mouseX, mouseY), entry),
						delta);
			}
		}
	}

	@Override
	protected void updateScrollingState(double mouseX, double mouseY, int button) {
		super.updateScrollingState(mouseX, mouseY, button);
		this.scrolling = button == 0 &&
				mouseX >= (double) this.getScrollbarPositionX() &&
				mouseX < (double) (this.getScrollbarPositionX() + 6);
	}

	@Override
	public boolean mouseClicked(double mouseX, double mouseY, int button) {
		this.updateScrollingState(mouseX, mouseY, button);
		if (!this.isMouseOver(mouseX, mouseY)) {
			return false;
		} else {
			POptionEntry entry = this.getEntryAtPos(mouseX, mouseY);
			if (entry != null) {
				if (entry.mouseClicked(mouseX, mouseY, button)) {
					this.setFocused(entry);
					this.setDragging(true);
					return true;
				} else if (button == 0) {
					this.clickedHeader((int) (mouseX - (double) (this.left + this.width / 2 - this.getRowWidth() / 2)),
							(int) (mouseY - (double) this.top) + (int) this.getScrollAmount() - 4);
				}
			}
		}

		return this.scrolling;
	}

	/**
	 * Gets entry at pos.
	 *
	 * @param x the x
	 * @param y the y
	 * @return the entry at pos
	 */
	public final POptionEntry getEntryAtPos(double x, double y) {
		int i = MathHelper.floor(y - (double) this.top) - this.headerHeight + (int) this.getScrollAmount() - 4;
		int index = i / this.itemHeight;
		return x < (double) this.getScrollbarPositionX() &&
				x >= (double) getRowLeft() &&
				x <= (double) (getRowLeft() + getRowWidth()) &&
				index >= 0 && i >= 0 &&
				index < this.getEntryCount() ? this.children().get(index) : null;
	}

	@Override
	protected int getScrollbarPositionX() {
		return this.width - 6;
	}

	@Override
	public int getRowWidth() {
		return this.width - (Math.max(0, this.getMaxPosition() - (this.bottom - this.top - 4)) > 0 ? 18 : 12);
	}

	@Override
	public int getRowLeft() {
		return left + 6;
	}

	/**
	 * Gets width.
	 *
	 * @return the width
	 */
	public int getWidth() {
		return width;
	}

	/**
	 * Gets top.
	 *
	 * @return the top
	 */
	public int getTop() {
		return this.top;
	}

	/**
	 * Gets parent.
	 *
	 * @return the parent
	 */
	public POptionsScreen getParent() {
		return parent;
	}

	@Override
	protected int getMaxPosition() {
		return super.getMaxPosition() + 4;
	}

	/**
	 * Gets displayed count.
	 *
	 * @return the displayed count
	 */
	public int getDisplayedCount() {
		return children().size();
	}
}
