package pm.j4.kerosene.gui;

import com.mojang.blaze3d.systems.RenderSystem;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.font.TextRenderer;
import net.minecraft.client.gui.widget.AlwaysSelectedEntryListWidget;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.text.StringVisitable;
import net.minecraft.text.Text;
import net.minecraft.text.TranslatableText;
import net.minecraft.util.Language;
import pm.j4.kerosene.util.module.ModuleBase;

/**
 * The type P option entry.
 */
@SuppressWarnings("deprecation")
public class POptionEntry extends AlwaysSelectedEntryListWidget.Entry<POptionEntry> {

	/**
	 * The Module.
	 */
	protected final ModuleBase module;
	/**
	 * The Client.
	 */
	protected final MinecraftClient client;
	/**
	 * The List.
	 */
	private final PModuleConfigurationWidget list;

	/**
	 * Instantiates a new P option entry.
	 *
	 * @param mod  the mod
	 * @param list the list
	 */
	public POptionEntry(ModuleBase mod, PModuleConfigurationWidget list) {
		this.module = mod;
		this.client = MinecraftClient.getInstance();
		this.list = list;
	}

	//TODO TEST move text to be centered
	@Override
	public void render(MatrixStack matrices, int index, int y, int x, int entryWidth, int entryHeight, int mouseX, int mouseY, boolean hovered, float tickDelta) {
		x += getXOffset();
		entryWidth -= getXOffset();
		RenderSystem.color4f(1.0F, 1.0F, 1.0F, 1.0F);
		Text name = this.getModName();
		StringVisitable nameString = name;
		int maxNameWidth = entryWidth - 32 - 3;
		TextRenderer font = this.client.textRenderer;
		if (font.getWidth(name) > maxNameWidth) {
			StringVisitable ellipse = StringVisitable.plain("...");
			nameString = StringVisitable.concat(font.trimToWidth(nameString, maxNameWidth - font.getWidth(ellipse)), ellipse);
		}

		font.draw(matrices, Language.getInstance().reorder(nameString), x + 32 + 3, y + ((float)entryHeight / 2), 0xFFFFFF);
	}

	@Override
	public boolean mouseClicked(double x, double y, int b) {
		this.list.select(this);
		return true;
	}

	/**
	 * Gets mod id.
	 *
	 * @return the mod id
	 */
	public String getModId() {
		return module.getModuleName();
	}

	/**
	 * Gets mod name.
	 *
	 * @return the mod name
	 */
	public TranslatableText getModName() {
		return module.getReadableName();
	}

	/**
	 * Gets x offset.
	 *
	 * @return the x offset
	 */
	@SuppressWarnings("SameReturnValue")
	public int getXOffset() {
		return 0;
	}
}
