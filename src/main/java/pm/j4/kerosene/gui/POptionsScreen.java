package pm.j4.kerosene.gui;

import com.mojang.blaze3d.systems.RenderSystem;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.gui.DrawableHelper;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.gui.screen.ScreenTexts;
import net.minecraft.client.gui.widget.ButtonWidget;
import net.minecraft.client.render.BufferBuilder;
import net.minecraft.client.render.Tessellator;
import net.minecraft.client.render.VertexFormats;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.text.StringVisitable;
import net.minecraft.text.Text;
import net.minecraft.text.TranslatableText;
import pm.j4.kerosene.util.config.ConfigManager;
import pm.j4.kerosene.util.data.ModInfoProvider;
import pm.j4.kerosene.util.module.ModuleBase;

/**
 * The type P options screen.
 */
@SuppressWarnings("deprecation")
public class POptionsScreen extends Screen {
	/**
	 * The Previous screen.
	 */
	private final Screen previousScreen;
	/**
	 * The Scroll percent.
	 */
	private double scrollPercent = 0;
	/**
	 * The Modules.
	 */
	private PModuleConfigurationWidget modules;
	/**
	 * The Config pane.
	 */
	private PModuleConfigPane configPane;
	/**
	 * The Selected.
	 */
	private POptionEntry selected;
	/**
	 * The Tooltip.
	 */
	private Text tooltip;

	/**
	 * The Pane y.
	 */
	private int paneY;
	/**
	 * The Right pane x.
	 */
	private int rightPaneX;

	/**
	 * Instantiates a new P options screen.
	 *
	 * @param previousScreen the previous screen
	 */
	public POptionsScreen(Screen previousScreen) {
		super(new TranslatableText("petroleum.options"));
		this.previousScreen = previousScreen;
	}

	@Override
	public void onClose() {
		super.onClose();
		if (this.client != null) {
			this.client.openScreen(previousScreen);
		}
	}

	protected void init() {
		paneY = 48;
		int paneWidth = this.width / 2 - 8;
		rightPaneX = width - paneWidth;
		this.modules = new PModuleConfigurationWidget(this.client,
				this.width - paneWidth,
				this.height,
				paneY + 19,
				this.height - 36,
				36,
				this.modules,
				this);
		this.modules.setLeftPos(0);
		this.children.add(this.modules);
		this.configPane = new PModuleConfigPane(this.client,
				paneWidth,
				this.height,
				paneY + 19,
				this.height - 36,
				48,
				this);
		this.configPane.setLeftPos(paneWidth);
		this.children.add(this.configPane);
		List<ModuleBase> configurableModules = ModInfoProvider.getRegisteredMods()
				.stream().filter(ModuleBase::configurable).collect(Collectors.toList());
		configurableModules.forEach(module -> this.modules.addEntry(new POptionEntry(module, this.modules)));
		this.addButton(new ButtonWidget(this.width / 2 - 75, this.height - 30, 150, 20, ScreenTexts.DONE, (buttonWidget) -> {
			ConfigManager.saveEverything();
			if (this.client != null) {
				this.client.openScreen(this.previousScreen);
			}
		}));
	}

	@Override
	public void render(MatrixStack matrices, int mouseX, int mouseY, float delta) {
		this.renderBackground(matrices);

		this.modules.render(matrices, mouseX, mouseY, delta);
		if (selected != null) {
			this.configPane.render(matrices, mouseX, mouseY, delta);
		}
		RenderSystem.disableBlend();
		drawTextWithShadow(matrices, this.textRenderer, this.title, this.modules.getWidth() / 2, 8, 16777215);
		super.render(matrices, mouseX, mouseY, delta);

		if (selected != null) {
			int offset = 36;
			int x = rightPaneX;
			int maxNameWidth = this.width - (x + offset);
			int lineSpacing = textRenderer.fontHeight + 1;
			Text name = selected.getModName();

			StringVisitable trimmedName = name;

			if (textRenderer.getWidth(name) > maxNameWidth) {
				StringVisitable ellipsis = StringVisitable.plain("...");
				trimmedName = StringVisitable.concat(textRenderer.trimToWidth(name, maxNameWidth - textRenderer.getWidth(ellipsis)), ellipsis);
			}
			if (mouseX > x + offset && mouseY > paneY + 1 && mouseY < paneY + 1 + textRenderer.fontHeight && mouseX < x + offset + textRenderer.getWidth(trimmedName)) {
				//TODO tooltip
				//selected.getModName()
			}
			textRenderer.draw(matrices, selected.getModName(), x + offset, paneY + 2 + lineSpacing, 0x808080);

			if (this.tooltip != null) {
				this.renderOrderedTooltip(matrices, textRenderer.wrapLines(this.tooltip, Integer.MAX_VALUE), mouseX, mouseY);
			}
		}
	}

	/**
	 * Sets tooltip.
	 *
	 * @param tooltip the tooltip
	 */
	private void setTooltip(Text tooltip) {
		this.tooltip = tooltip;
	}

	@Override
	public void renderBackground(MatrixStack matrices) {
		POptionsScreen.overlayBackground(this.width, this.height);
	}

	/**
	 * Overlay background.
	 *
	 * @param x2 the x 2
	 * @param y2 the y 2
	 */
	static void overlayBackground(int x2, int y2) {
		Tessellator t_1 = Tessellator.getInstance();
		BufferBuilder buffer = t_1.getBuffer();
		Objects.requireNonNull(MinecraftClient.getInstance()).getTextureManager().bindTexture(DrawableHelper.OPTIONS_BACKGROUND_TEXTURE);
		RenderSystem.color4f(1.0F, 1.0F, 1.0F, 1.0F);
		buffer.begin(7, VertexFormats.POSITION_TEXTURE_COLOR);
		buffer.vertex(0, y2, 0.0D).texture(0 / 32.0F, y2 / 32.0F).color(64, 64, 64, 255).next();
		buffer.vertex(x2, y2, 0.0D).texture(x2 / 32.0F, y2 / 32.0F).color(64, 64, 64, 255).next();
		buffer.vertex(x2, 0, 0.0D).texture(x2 / 32.0F, 0 / 32.0F).color(64, 64, 64, 255).next();
		buffer.vertex(0, 0, 0.0D).texture(0 / 32.0F, 0 / 32.0F).color(64, 64, 64, 255).next();
		t_1.draw();
	}

	/**
	 * Gets scroll percent.
	 *
	 * @return the scroll percent
	 */
	double getScrollPercent() {
		return scrollPercent;
	}

	/**
	 * Update scroll percent.
	 *
	 * @param scrollPercent the scroll percent
	 */
	void updateScrollPercent(double scrollPercent) {
		this.scrollPercent = scrollPercent;
	}

	/**
	 * Gets selected.
	 *
	 * @return the selected
	 */
	POptionEntry getSelected() {
		return selected;
	}

	/**
	 * Update selected.
	 *
	 * @param entry the entry
	 */
	void updateSelected(POptionEntry entry) {
		if (entry != null) {
			this.selected = entry;
		}
	}
}
