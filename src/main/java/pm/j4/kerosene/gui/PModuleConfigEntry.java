package pm.j4.kerosene.gui;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.gui.Element;
import net.minecraft.client.gui.widget.AbstractButtonWidget;
import net.minecraft.client.gui.widget.ButtonWidget;
import net.minecraft.client.gui.widget.ElementListWidget;
import net.minecraft.client.options.KeyBinding;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.text.LiteralText;
import net.minecraft.text.Text;
import pm.j4.kerosene.util.config.ConfigHolder;
import pm.j4.kerosene.util.config.ConfigManager;
import pm.j4.kerosene.util.module.ModuleFunction;
import pm.j4.kerosene.util.module.option.BooleanOption;
import pm.j4.kerosene.util.module.option.ConfigurationOption;
import pm.j4.kerosene.util.module.option.KeybindOption;
import pm.j4.kerosene.util.module.option.ListOption;

/**
 * The type P module config entry.
 */
public class PModuleConfigEntry extends ElementListWidget.Entry<PModuleConfigEntry> {
	/**
	 * The Option.
	 */
	protected ConfigurationOption option;
	/**
	 * The Display text.
	 */
	protected final Text displayText;

	/**
	 * The Parent.
	 */
	private final PModuleConfigPane parent;

	/**
	 * The Elements.
	 */
	private final List<Element> elements = new ArrayList<>();

	/**
	 * The True value.
	 */
	private final String trueValue;
	/**
	 * The False value.
	 */
	private final String falseValue;

	/**
	 * The Selected.
	 */
	private Element selected;

	/**
	 * Instantiates a new P module config entry.
	 *
	 * @param option the option
	 * @param text   the text
	 * @param parent the parent
	 */
	public PModuleConfigEntry(ConfigurationOption option, Text text, PModuleConfigPane parent) {
		this.option = option;
		this.displayText = text;
		this.parent = parent;
		this.trueValue = "Yes";
		this.falseValue = "No";
	}

	/**
	 * Instantiates a new P module config entry.
	 *
	 * @param option     the option
	 * @param text       the text
	 * @param parent     the parent
	 * @param trueValue  the true value
	 * @param falseValue the false value
	 */
	public PModuleConfigEntry(ConfigurationOption option, Text text, PModuleConfigPane parent, String trueValue, String falseValue) {
		this.option = option;
		this.displayText = text;
		this.parent = parent;
		this.trueValue = trueValue;
		this.falseValue = falseValue;
	}

	@Override
	public List<? extends Element> children() {
		return elements;
	}

	@Override
	public boolean mouseClicked(double mouseX, double mouseY, int button) {
		if (this.isMouseOver(mouseX, mouseY)) {
			this.parent.setSelected(this);
			System.out.println(displayText.getString() + " clicked");
			String className = option.getClass().toString();
			elements.forEach((widget) -> {
				if (widget.mouseClicked(mouseX, mouseY, button)) {
					System.out.println("Button clicked");
					selected = widget;
				}
			});
			return true;
		}
		return false;
	}

	@Override
	public boolean mouseReleased(double mouseX, double mouseY, int button) {
		return this.isMouseOver(mouseX, mouseY);
	}

	@Override
	public boolean keyPressed(int keyCode, int scanCode, int modifiers) {
		if(this.selected != null) {
			return this.selected.keyPressed(keyCode, scanCode, modifiers);
		}
		return false;
	}

	@Override
	public boolean keyReleased(int keyCode, int scanCode, int modifiers) {
		if(this.selected != null) {
			return this.selected.keyReleased(keyCode, scanCode, modifiers);
		}
		return false;
	}

	@Override
	public void render(MatrixStack matrices, int index, int y, int x, int entryWidth, int entryHeight, int mouseX, int mouseY, boolean hovered, float tickDelta) {
		if (this.displayText != null) {
			MinecraftClient.getInstance().textRenderer.drawWithShadow(matrices, displayText, x, y, 0xAAAAAA);
		}
		if (this.option != null) {
			//TODO option text box (?)
			// option should be centered or otherwise offset
			// but not extend past the side of the pane
			int fontHeight = MinecraftClient.getInstance().textRenderer.fontHeight;
			//TODO use TranslatableText instead of LiteralText
			MinecraftClient.getInstance().textRenderer.drawWithShadow(matrices, new LiteralText(option.getDescription() + " " + option.getStringValue()), x, y + fontHeight + 4, 0xFFFFFF);

			if(elements.size() == 0) {
				String className = option.getClass().toString();
				if (className.equals(BooleanOption.class.toString())) {
					elements.add(new ButtonWidget(x, y + (int)(fontHeight * 2.5),
							entryWidth,
							fontHeight * 2,
							new LiteralText(((BooleanOption)option).getValue() ? this.trueValue : this.falseValue), (button) -> {
						button.setMessage(new LiteralText((!((BooleanOption)option).getValue()) ? this.trueValue : this.falseValue));
						BooleanOption newValue = new BooleanOption(option.getConfigKey(), option.getDescription(), option.getParent());
						newValue.setValue((!((BooleanOption)option).getValue()));
						option.getParent().updateConfigOption(newValue.getConfigKey(), newValue);
						this.option = newValue;
					}));
				}
				else if (className.equals(ListOption.class.toString())) {
					// TODO: determine whether list options are viable,
					//  considering that it would be easier to split lists into multiple PModuleConfigEntries
					System.out.println("NYI list");
				}
				else if (className.equals(KeybindOption.class.toString())) {
					ButtonWidget bindButton = new ButtonWidget(x, y + (int)(fontHeight * 2.5),
							entryWidth,
							fontHeight * 2,
							new LiteralText(option.getStringValue().toUpperCase()), (button) -> button.setMessage(new LiteralText("Press any key..."))) {
						@Override
						public boolean keyPressed(int keyCode, int scanCode, int modifiers) {
							if (this.active && this.visible) {
								//TODO
								if (keyCode != 257 && keyCode != 32 && keyCode != 335) {
									KeybindOption newValue = new KeybindOption(option.getConfigKey(), option.getDescription(), option.getParent());
									KeyBinding bind = new KeyBinding(((KeybindOption)option).getTranslationKey(), keyCode, "category." + option.getParent().getParent());
									System.out.println("looking for binds in "
											+ option.getParent().getModuleName()
											+ " that match the name "
											+ ((KeybindOption)option).getBindingInfo().attachedFunctionName);
									List<ModuleFunction> foundBinds = option.getParent().matchBinding(((KeybindOption)option).getBindingInfo().attachedFunctionName);
									if (foundBinds.size() > 0) {
										newValue.fromKeybind(bind, foundBinds.get(0));
										Optional<ConfigHolder> config = ConfigManager.getConfig(option.getParent().getParent());
										if (config.isPresent()) {
											config.get().globalConfig.setBinding(bind, foundBinds.get(0));
											option = newValue;
											this.setMessage(new LiteralText(option.getStringValue().toUpperCase()));
										}
										selected = null;
										return false;
									}
									else {
										System.out.println("no binds found to set!");
										selected = null;
										return true;
									}
								} else {
									this.playDownSound(MinecraftClient.getInstance().getSoundManager());
									this.onPress();
									return true;
								}
							} else {
								return false;
							}
						}
					};
					elements.add(bindButton);
				}
				else {
					System.out.println("NYI other/string");
					//TODO
				}
			}
			else {
				elements.forEach((widget) -> {
					if (widget instanceof AbstractButtonWidget) {
						((AbstractButtonWidget)widget).render(matrices, mouseX, mouseY, tickDelta);
					}
				});
			}
		}
	}
}