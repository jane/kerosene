package pm.j4.kerosene;

import java.util.Map;
import java.util.Optional;
import net.fabricmc.api.ModInitializer;
import net.fabricmc.fabric.api.client.event.lifecycle.v1.ClientTickEvents;
import net.minecraft.client.options.KeyBinding;
import pm.j4.kerosene.modules.ExampleModule;
import pm.j4.kerosene.modules.bindings.BindingManager;
import pm.j4.kerosene.modules.bindings.ChatCommands;
import pm.j4.kerosene.util.config.ConfigHolder;
import pm.j4.kerosene.util.config.ConfigManager;
import pm.j4.kerosene.util.config.Module;
import pm.j4.kerosene.util.data.ModInfoProvider;
import pm.j4.kerosene.util.module.ModuleBase;
import pm.j4.kerosene.util.module.ModuleFunction;


/**
 * The type Kerosene mod.
 */

@Module(ExampleModule.class)
@Module(BindingManager.class)
@Module(ChatCommands.class)
public class KeroseneMod implements ModInitializer {
	@Override
	public void onInitialize() {

		ConfigManager.initConfig("kerosene", KeroseneMod.class);

		Optional<ConfigHolder> conf = ConfigManager.getConfig("kerosene");


		//initialize any keybinds, data, etc.
		ModInfoProvider.getRegisteredMods().forEach(ModuleBase::init);

		//initialize keybind handler
		ClientTickEvents.END_CLIENT_TICK.register(client -> {
			if (ModInfoProvider.client != client) {
				ModInfoProvider.client = client;
			}
			Map<KeyBinding, ModuleFunction> binds = BindingManager.getActiveKeybinds();
			binds.forEach((bind, func) -> {
				while (bind.wasPressed()) {
					Optional<ConfigHolder> config = ConfigManager.getConfig(func.getParent().getParent());
					config.ifPresent(configHolder -> {
						Map<KeyBinding, ModuleFunction> bindings = configHolder.globalConfig.bindings;
						if (bindings.containsKey(bind)) {
							bindings.get(bind).activate(client);
						}
						else {
							System.out.println("binding function no longer present");
							BindingManager.removeBind(bind);
						}
					});
				}
			});
		});

		//save any defaults that have been set
		ConfigManager.saveEverything();
	}
}
